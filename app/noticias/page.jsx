
"use client"
import MainService  from "@/service/MainService";
import { Container, Card, Typography, CardContent, CardActions, Button, Divider } from "@mui/material"
import { useEffect, useState } from "react";
import axios from 'axios';


function Noticiaspage() {

    const [noticia, setNoticia] = useState([]);

    const fetchData = () => {
        return axios.get(`http://localhost:3001/portal/news`)
        .then((response)=> setNoticia(response.data));
    }

    useEffect(() =>{
        fetchData();
    }, [])

     
    return (
        <Container>
            <div>
                <h1>Noticias</h1>
                {noticia.map(noti => (  
                <div> 
                    <Card>
                        <CardContent>
                            <Typography variant="h5">
                            {noti.tituloPublicacion}
                            </Typography>
                            <Typography 
                            component="p"
                            variant="body2"
                            >
                            {noti.contenidoPublicacion}            
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button 
                                variant="contained"
                                color="warning"
                            >Editar</Button>
                            <Button 
                                variant="contained"
                                color="error"
                                >Eliminar
                            </Button>
                        </CardActions>
                    </Card>
                    <Divider orientation="vertical" variant="middle" flexItem />
                </div>
                ))}
            </div>

        </Container>
      
    )
  }
  
  export default Noticiaspage


