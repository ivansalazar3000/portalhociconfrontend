import FormRegistro from '@/components/FormRegistro'
import React from 'react'

function createNoticia() {
  return (
    <div>
        <h1>Crear Noticia</h1>
        <FormRegistro />
    </div>
  )
}

export default createNoticia