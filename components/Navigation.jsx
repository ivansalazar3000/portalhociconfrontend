import React from 'react'
import  Link  from 'next/link'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

function Navigation() {
  return (
    <>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Toolbar>
              
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Portal "HOCICON"
              </Typography>

              <Button color="inherit">
                <Link href='/'>Home</Link>
              </Button>
              <Button color="inherit">
                <Link href='/noticias'>Ultimas Noticias</Link>
              </Button>
              <Button color="inherit">
                <Link href='/create'>Crear Noticias</Link>
              </Button>
            </Toolbar>
          </AppBar>
        </Box>
    </>
  )
}

export default Navigation