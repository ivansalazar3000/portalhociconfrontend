
"use client"
import { useState } from 'react'
import axios from 'axios';


const FormRegistro= () => {

  const [noticia, setNoticia] = useState({
    tituloPublicacion: "",
    contenido: "",
    fecha: "",
    lugar: "",
  });
  const [msg, setMsg] = useState({
    message: "",
    color: "",
    visible: "no",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setNoticia({ ...noticia, [name]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
     await axios.post("http://localhost:3001/portal/news", noticia)
     .then(({data}) => console.log(data))
    
    setNoticia({ tituloPublicacion: "", contenidoPublicacion: "", fechaPublicacion: "", lugarPublicacion: "" });
  };
  
  return (
    <>
      <div className="container p-4">
        <h1 className="text-center">Nuevo Producto</h1>
        {msg.visible === "si" ? (
          <div className={"alert alert-" + msg.color} role="alert">
            {msg.message}
          </div>
        ) : (
          ``
        )}
        <div className="row p-4">
          <div className="col-7 mx-auto">
            <form className="form" onSubmit={handleSubmit} autoComplete="off">
              <div className="mb-2">
                <input
                  type="text"
                  required
                  onChange={handleChange}
                  value={noticia.tituloPublicacion}
                  autoFocus
                  className="form-control"
                  placeholder="Introduzca titulo"
                  name="tituloPublicacion"
                />
              </div>
              <div className="mb-2">
                <textarea
                  required
                  onChange={handleChange}
                  value={noticia.contenidoPublicacion}
                  rows="2"
                  className="form-control"
                  name="contenidoPublicacion"
                  placeholder="Introduzca contenido"
                ></textarea>
              </div>
              <div className="mb-2">
                <input
                  type='date'
                  required
                  onChange={handleChange}
                  value={noticia.fechaPublicacion}
                  className="form-control"
                  name="fechaPublicacion"
                />
              </div>
              <div className="mb-2">
              <select  
                    value={noticia.lugarPublicacion}
                    onChange={handleChange}
                    name="lugarPublicacion"
                    >
                    <option value="la_paz">La Paz</option>
                    <option value="cochabamba">Cochabamba</option>
                    <option value="santa_cruz">Santa Cruz</option>
                </select>
              </div>
              <div className="mb-2 mt-4">
                <button className="btn btn-outline-success w-100">
                  Guardar noticia
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
export default FormRegistro;
