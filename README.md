# Nombre del Proyecto

Prueba Practica para pasantias en AGETIC
Portal de noticias el Hocicon

Frontend-Started: Frontend del portal de noticias el Hocicon
Desarrollado en NextJS
MUI para estilos

## Requisitos Previos

Asegurarse de tener instalado node.js y npm.

- Node.js: [Descargar e Instalar Node.js](https://nodejs.org/)
- npm: (Normalmente se instala automáticamente con Node.js)

## Contribuciones

Cualquier contribución o comentario es bienvenido. 

## Consultas

ivansalazar3000@gmail.com
cel:79594995

## Licencia

Este es solo un ejemplo básico y puedes agregar secciones adicionales según las necesidades de tu proyecto, como instrucciones de configuración específicas, información sobre cómo contribuir, detalles sobre la estructura del proyecto, entre otros.
