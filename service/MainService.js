import axios from 'axios'


export default class MainService{

    constructor(){
        const URL = "http://localhost:3001/"
        axios.defaults.baseURL= URL;
        axios.defaults.common = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }

    listarNoticias(){
        return axios.get("portal/news");
    }



}