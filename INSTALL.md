## Instalacion del instalacion del Frontend

## Requisitos Previos

Asegúrate de tener instalados los siguientes requisitos en tu máquina:

- Node.js: [Descargar e Instalar Node.js](https://nodejs.org/)
- npm: (Normalmente se instala automáticamente con Node.js)


## Instrucciones de Instalación

A continuación, se detallan los pasos para clonar, configurar y ejecutar el proyecto de Next.js en tu entorno local.

## clonar repositorio

- git clone https://gitlab.com/ivansalazar3000/portalhociconfrontend

## Instalar dependencia
Ejecutar en a terminal:
- npm install


## ejecutar proyecto
 Para ejecutar el proyecto ejecute:
- npm run dev
